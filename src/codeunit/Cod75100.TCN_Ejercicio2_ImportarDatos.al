// codeunit 75100 "TCN_Ejercicio2_ImportarDatos"
// {
//     procedure ImportarPresencia()
//     var
//         rlTempblobTMP: Record TempBlob temporary;
//         xlInStream: InStream;
//         xLinea: Text;
//         xlFichero: Text;
//         xlNumCampo: Integer;
//         XlCampo: text;
//         xlNumLinea: Integer;
//         rlHistoricoPresencia: Record "Historico Presencia";
//         rlTempCommentLine: Record "Comment Line" temporary;
//         rlConfiguracionPresencia: Record "Configuracion Presencia";
//     begin
//         rlConfiguracionPresencia.GetF();
//         rlConfiguracionPresencia.TestField(SeparadorImp);
//         rlTempblobTMP.Blob.CreateInStream(xlInStream, TextEncoding::Windows); //TexEnconding para que acepte bien los acentos
//         if UploadIntoStream('Seleccione fichero',
//                             '', 'Archivos de texto (*.txt, *.csv)|*.txt;*.csv|todos los archivos(*.*)|*.*',
//                             xlFichero, xlInStream) then begin
//             while not xlInStream.EOS do begin
//                 xlInStream.ReadText(xLinea);
//                 xlNumCampo := 0;
//                 xlNumLinea += 1;
//                 if (xlNumLinea <> 1) then begin
//                     rlHistoricoPresencia.Init();
//                     if AsignarDatosF(rlHistoricoPresencia, xLinea, rlconfiguracionPresencia.SeparadorImp) then begin
//                         rlHistoricoPresencia.Insert(true);
//                     end else begin
//                         rlTempCommentLine.Init();
//                         rlTempCommentLine."Line No." := xlNumLinea;
//                         rlTempCommentLine.Comment := CopyStr(StrSubstNo('Lin %1, error %2', rlHistoricoPresencia.NOrden, GetLastErrorText),
//                                                                                 1, MaxStrLen(rlTempCommentLine.Comment));
//                         rlTempCommentLine.Insert(false);
//                     end;
//                 end;
//             end;
//         end else begin
//             Error('No se puede subir el fichero');
//         end;
//         if rlTempCommentLine."Line No." = 0 then begin
//             Message('Proceso finalizado');
//         end else begin
//             page.Run(0, rlTempCommentLine);
//         end;
//     end;


// }