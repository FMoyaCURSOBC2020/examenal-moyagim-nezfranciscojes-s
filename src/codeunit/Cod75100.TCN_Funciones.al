codeunit 75100 "TCN_Funciones"
{
    procedure CrearPaginaF()
    var
        pgTCNEntradaDatosEjercicio1: Page TCN_EntradaDatos_Ejercicio1;

    begin
        //Ejercicio 1
        pgTCNEntradaDatosEjercicio1.SetDatosF('Nombre1:', 'aaaa');
        pgTCNEntradaDatosEjercicio1.SetDatosF('Nombre2:', '');
        pgTCNEntradaDatosEjercicio1.SetDatosF('Num1:', 0);
        pgTCNEntradaDatosEjercicio1.SetDatosF('Num2:', 0);
        pgTCNEntradaDatosEjercicio1.SetDatosF('Fecha Inicio:', 0D);
        pgTCNEntradaDatosEjercicio1.SetDatosF('Fecha Fin:', 0D);

        pgTCNEntradaDatosEjercicio1.GetDatosF(1, 'aaaa');
        pgTCNEntradaDatosEjercicio1.GetDatosF(2, 'Nombre2:');
        pgTCNEntradaDatosEjercicio1.GetDatosF(3, 'num1');
        pgTCNEntradaDatosEjercicio1.GetDatosF(4, 'Num2');
        pgTCNEntradaDatosEjercicio1.GetDatosF(5, 'Fecha Inicio:');
        pgTCNEntradaDatosEjercicio1.GetDatosF(6, 'Fecha Fin:');

        if pgTCNEntradaDatosEjercicio1.RunModal() in [Action::LookupOK, Action::OK] then begin
        end;
    end;


    //Ejercicio3

    local procedure ExisteCodigoF(pCadenaFiltro: Text; pCodigo: Text)
    var
        rtTempCommentLine: Record "Comment Line" temporary;
        xError: Label 'Existe la cadena';

    begin
        rtTempCommentLine.SetRange("No.", pCadenaFiltro);
        rtTempCommentLine.SetRange(Code, pCodigo);
        if (rtTempCommentLine.FindFirst()) then begin
            Message(xError);
        end else begin
            Error('El campo no existe en la cadena');
        end;
    end;

}