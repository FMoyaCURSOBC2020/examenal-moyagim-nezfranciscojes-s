page 75100 "TCN_EntradaDatos_Ejercicio1"

{
    Caption = 'Ejercicio 1 - Entrada de datos';
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;

    layout
    {
        area(Content)
        {
            group(Datos)
            {
                field(Nombre1; xArrayTexto[1])
                {
                    Caption = 'Primer nombre';
                    ApplicationArea = All;
                    CaptionClass = xArrayCaption[1];
                    Visible = xVisibleNom1;
                }

                field(Nombre2; xArrayTexto[2])
                {
                    Caption = 'Segundo nombre';
                    ApplicationArea = All;
                    CaptionClass = xArrayCaption[2];
                    Visible = xVisibleNom2;
                }

                field(Numero1; xArrayNum[1])
                {
                    Caption = 'Primer número';
                    ApplicationArea = All;
                    CaptionClass = xArrayCaption[3];
                    Visible = xVisibleNom3;
                }

                field(Numero2; xArrayNum[2])
                {
                    Caption = 'Segundo número';
                    ApplicationArea = All;
                    CaptionClass = xArrayCaption[4];
                    Visible = xVisibleNom4;
                }

                field(Fecha1; xArrayFecha[1])
                {
                    Caption = 'Primera fecha';
                    ApplicationArea = All;
                    CaptionClass = xArrayCaption[5];
                    Visible = xVisibleNom5;
                }

                field(Fecha2; xArrayFecha[2])
                {
                    Caption = 'Segunda fecha';
                    ApplicationArea = All;
                    CaptionClass = xArrayCaption[6];
                    Visible = xVisibleNom6;
                }
            }
        }
    }

    procedure GetDatosF(pPosicion: Integer; pTexto: Text)
    begin

        pTexto := xArrayTexto[pPosicion];

    end;

    procedure GetDatosF(pPosicion: Integer; pNum: Decimal)
    begin

        pNum := xArrayNum[pPosicion];

    end;

    procedure GetDatosF(pPosicion: Integer; pFecha: Date)
    begin

        pFecha := xArrayFecha[pPosicion];

    end;

    local procedure SetDatosF(pCaption: Text; pTexto: Text)
    var
        contValores: integer;
    begin
        for contValores := 1 to 2 do begin
            if xArrayCaption[contValores] = '' then
                xVisibleNom1 := false;
            xArrayCaption[contValores] += xArrayCaption[contValores];

            if xArrayCaption[contValores] <> '' then
                xArrayCaption[contValores] := pCaption;
            xArrayTexto[contValores] := pTexto;
            xVisibleNom2 := true;
            xArrayCaption[contValores] += xArrayCaption[contValores];
        end;
    end;

    procedure SetDatosF(pCaption: Text; pNum: Decimal)
    var
        contValores: integer;
    begin
        for contValores := 3 to 4 do begin
            if xArrayCaption[contValores] = '' then
                xVisibleNom3 := false;
            xArrayCaption[contValores] += xArrayCaption[contValores];

            if xArrayCaption[contValores] <> '' then
                xArrayCaption[contValores] := pCaption;
            xArrayNum[contValores] := pNum;
            xVisibleNom4 := true;
            xArrayCaption[contValores] += xArrayCaption[contValores];
        end;
    end;

    procedure SetDatosF(pCaption: Text; pFecha: Date)
    var
        contValores: integer;
    begin
        for contValores := 5 to 6 do begin
            if xArrayCaption[contValores] = '' then
                xVisibleNom5 := false;
            xArrayCaption[contValores] += xArrayCaption[contValores];

            if xArrayCaption[contValores] <> '' then
                xArrayCaption[contValores] := pCaption;
            xArrayFecha[contValores] := pFecha;
            xVisibleNom6 := true;
            xArrayCaption[contValores] += xArrayCaption[contValores];
        end;
    end;

    var
        xNombre1: Text;
        xNombre2: Text;
        xNumero1: Decimal;
        xNumero2: Decimal;
        xFecha1: Date;
        xFecha2: Date;
        xVisibleNom1: Boolean;
        xVisibleNom2: Boolean;
        xVisibleNom3: Boolean;
        xVisibleNom4: Boolean;
        xVisibleNom5: Boolean;
        xVisibleNom6: Boolean;

        xArrayCaption: array[6] of Text[80];
        xArrayTexto: array[2] of Text;
        xArrayNum: array[2] of Decimal;
        xArrayFecha: array[2] of Date;
}